---
layout: page
title: About
permalink: /about/
---

**Hei, I'm Alexander Alemayhu.**

Currently I work as a software engineer on the [Cilium][0] project, but I
intend to become a kernel network developer.

[0]: https://www.cilium.io/
